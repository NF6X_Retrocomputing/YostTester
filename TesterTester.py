#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of YostTester.
#
#  YostTester is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  YostTester is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with YostTester.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Script for testing the LEDs on a YostTester."""

import serial
import sys
import time


if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.stderr.write('USAGE: {:s} <port>\n'.format(sys.argv[0]))
        exit(1)

    port = serial.Serial(port=sys.argv[1], baudrate=9600, timeout=None)

    while True:
        port.sendBreak(0.25)
        port.setDTR(False)
        time.sleep(0.25)
        port.setDTR(True)
        port.setRTS(False)
        time.sleep(0.25)
        port.setRTS(True)
        time.sleep(0.25)
